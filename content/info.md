---
menu:
  main:
    weight: 20
title: Info
---

En los streams de Molotec nos dedicamos a programar y transmitir en vivo el desarrollo de distintos proyectos.

Estamos todos los miércoles en [twitch.tv/molotec](https://twitch.tv/molotec).
